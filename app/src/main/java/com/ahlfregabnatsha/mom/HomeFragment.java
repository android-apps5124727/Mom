package com.ahlfregabnatsha.mom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;


//import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class HomeFragment extends Fragment {

    private Activity referenceActivity;
    private View parentHolder;
    Button taskSpawner;
    private int taskRemoveDelay = 1500;
    Button taskCreateTest;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        referenceActivity = getActivity();
        parentHolder = inflater.inflate(R.layout.fragment_home, container,
                false);

        taskSpawner = (Button) parentHolder.findViewById(R.id.spawnTask);

        taskSpawner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewTask();
            }
        });

        taskCreateTest = (Button) parentHolder.findViewById(R.id.addTask);
        taskCreateTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(referenceActivity, TaskCreate.class);
                startActivity(intent);

            }
        });

        return parentHolder;
    }

    public void addNewTask(){
        LinearLayout layout = (LinearLayout) parentHolder.findViewById(R.id.taskList);
        Task task = new Task(getActivity());
        task.setText("new task");
        task.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                initTaskRemoval(layout, task);
            }
        });
        task.init();
        layout.addView(task);
    }

    /**
     *
     */
    public void initTaskRemoval(LinearLayout layout, Task task){
        CountDownTimer timer = new CountDownTimer(taskRemoveDelay, 10) {
            @Override
            public void onTick(long millisUntilFinished){
                task.setAlpha(task.getAlpha()-((float)0.01));
                if (task.getAlpha() <0)
                    layout.removeView(task);
            }

            @Override
            public void onFinish() {
                layout.removeView(task);
            }
        }.start();
    }
}
