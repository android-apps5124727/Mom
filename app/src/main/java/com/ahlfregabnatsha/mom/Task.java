package com.ahlfregabnatsha.mom;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class Task extends androidx.appcompat.widget.AppCompatCheckBox {
    private Task task = this;

    private final String colorGood      = "#8859FF6A";
    private final String colorMedGood   = "#9AE5FF52";
    private final String colorMedium    = "#9AFFF200";
    private final String colorMedBad    = "#9AFF9900";
    private final String colorBad       = "#A4FF2600";

    private int maxTime = 10000;
    private int secondsPassed = 0;
    private int taskTime = 2;           // Time in seconds the user has to complete a task before worsening mom's temper.

    public Task(Context context) {
        super(context);
    }

    public void init() {
        task.setBackgroundColor(Color.parseColor(colorGood));

        CountDownTimer counter = new CountDownTimer(maxTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                secondsPassed++;
                if (secondsPassed > taskTime*4){
                    task.setBackgroundColor(Color.parseColor(colorBad));
                }
                else if (secondsPassed > taskTime*3){
                    task.setBackgroundColor(Color.parseColor(colorMedBad));
                }
                else if (secondsPassed >taskTime*2){
                    task.setBackgroundColor(Color.parseColor(colorMedium));
                }
                else if (secondsPassed > taskTime){
                    task.setBackgroundColor(Color.parseColor(colorMedGood));
                }
            }

            @Override
            public void onFinish() {
            }
        }.start();


    }

}